import { Grid } from "@mui/material";
import { MapView } from "../components/map/MapView";
import { SearchLayer } from "../components/search/SearchLayer";
import { IMapTestService } from "../hooks/useMapControler";

export interface LayoutAppMap{
    controlerMap:IMapTestService;
}

export function LayoutApp(props:LayoutAppMap){


    return (<Grid container spacing={2}   columns={16}>
        <Grid item  xl={12} >
          <MapView {...props.controlerMap} />
        </Grid>
        <Grid item xl={4} height={1200}>
          <SearchLayer {...props.controlerMap}  />
        </Grid>
      </Grid>)
}