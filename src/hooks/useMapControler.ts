import { useEffect, useState } from "react";
import { restaurants } from "./dataExample";
  
// Contrat d'interface du controleur 
export interface IMapTestService{
    filterNom : string;
    setFilterNom: React.Dispatch<React.SetStateAction<string>>;
    filterSMS : string;
    setFilterSMS: React.Dispatch<React.SetStateAction<string>>;
    filtersSMSList:string[];
    dataFiltered : GeoJSON.FeatureCollection<GeoJSON.Geometry>;
}

// Controleur de l'application
export function useMapControler ():IMapTestService {

    
    // Adapteur du fichier json (créer avec un outil externe depuis le fichier csv)
    // en objet GeoJson exploitable
    const translateToGeoJson = ():GeoJSON.FeatureCollection<GeoJSON.Geometry> =>{
        let newGJsonData : GeoJSON.FeatureCollection<GeoJSON.Geometry> = {
            type:"FeatureCollection",
            features: [],
        };

        restaurants.forEach((res)=>{
            // On traite seulement les établissements possédant une longitude et une latitude
            if(res.Longitude !== "" || res.Latitude !== ""  )
                newGJsonData.features.push({
                    properties: res,
                    geometry: {
                        type: "Point",
                        coordinates: [parseFloat(res.Longitude.split(',')[0]+"."+res.Longitude.split(',')[1]),
                        parseFloat(res.Latitude.split(',')[0]+"."+res.Latitude.split(',')[1])]
                    },
                    type: "Feature"
                })
        })
         return newGJsonData;
    }

    // Etat de gestion de la liste de restaurant
    const [dataFiltered,setDataFiltered]  = useState<GeoJSON.FeatureCollection<GeoJSON.Geometry>>(translateToGeoJson());

    // Etat des 2 filtres (nom de l'etablissement et Simplified Market Segment)
    const [filterNom,setFilterNom] = useState<string>("");
    const [filterSMS,setFilterSMS ] = useState<string>("");


    // Liste d'option issue 
    const [filtersSMSList,setFiltersSMSList] = useState<string[]>([]);

    // Fonction de filtrage qui génére une nouvelle liste de restaurants en fonction des filtres utilisés
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const filter = () => {
        
        let newRestaurantsList : GeoJSON.Feature<GeoJSON.Geometry, GeoJSON.GeoJsonProperties>[]= [];
        // Sans filtre, on regenere la liste d'origine
        if(filterNom === "" && filterSMS === ""){
            setDataFiltered(translateToGeoJson());
            return 
        }
        
        dataFiltered.features.forEach((datum)=>{
            if(datum.properties){
                // Filtre sur le nom
                if(filterNom !== "" && datum.properties['Business Name'].toLowerCase().includes(filterNom.toLowerCase())){
                    console.log(datum.properties['Simplified Menu']);
                    newRestaurantsList.push(datum);
                }
                if(filterSMS !== "" && filterSMS === datum.properties['Simplified Market Segment (GFC2)']){
                    console.log(filterSMS,datum.properties['Simplified Market Segment (GFC2)']);
                    newRestaurantsList.push(datum);
                }
            }
        });
        
        setDataFiltered({...dataFiltered,features:newRestaurantsList});
        return
    }

    // Création de la liste d'options à l'initialisation du composant
    useEffect( ()=>{

        let filtersList : string[] = [];
        restaurants.forEach((rest)=>{
            if(!filtersList.includes(rest["Simplified Market Segment (GFC2)"]))
            filtersList.push(rest["Simplified Market Segment (GFC2)"]);
        });
        setFiltersSMSList(filtersList);
    },[])

    // Lancement du filter à chaque modification de filtre
    useEffect( ()=>{
        filter();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[ filterNom, filterSMS])

    // Mise à disposition des variables et fonctions pour les composants visuelles
    return {
        filterNom:filterNom,
        setFilterNom:setFilterNom,
        filterSMS:filterSMS,
        setFilterSMS:setFilterSMS,
        filtersSMSList:filtersSMSList,
        dataFiltered:dataFiltered
    };
}