import './App.css';
import { useMapControler } from './hooks/useMapControler';
import { LayoutApp } from './layout/LayoutApp';

function App() {

  const controler = useMapControler();

  return (
    <div className="App">
      
      <LayoutApp controlerMap={controler}  />
    </div>
  );
}

export default App;
