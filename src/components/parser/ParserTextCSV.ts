import Papa, { ParseConfig } from 'papaparse';


export class ParserTextCSV{

    config: ParseConfig;
    constructor(){
        this.config = this.setDefaultConfig();
    }

    setDefaultConfig ():ParseConfig {
        return  {
            delimiter: ",",	// auto-detect
            newline: undefined,	// auto-detect
            quoteChar: '"',
            escapeChar: '"',
            header: false,
            transformHeader: undefined,
            dynamicTyping: false,
            preview: 0,
            comments: false,
            step: undefined,
            complete: undefined,
            skipEmptyLines: false,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            transform: undefined,
            delimitersToGuess: [',', '\t', '|', ';', Papa.RECORD_SEP, Papa.UNIT_SEP]
        }
        
    }

    parse(csvString: string){
        Papa.parse(csvString,this.config)
    }

    parseWithoutConfig(csvString:string){
        Papa.parse(csvString);
    }

}