import {  Grid, InputLabel, MenuItem, OutlinedInput, Select, TextField } from "@mui/material";
import { IMapTestService } from "../../hooks/useMapControler";

export function SearchLayer(props:IMapTestService){
    return (<Grid
    container
    direction="column"
    justifyContent="space-between"
    spacing={2}
    alignItems="center">
      <Grid  item >
        <TextField fullWidth id="standard-basic" label="Nom du restaurant" variant="standard" onChange={(e)=> {
          props.setFilterNom(e.target.value);
          }} />
      </Grid>
      <Grid item >
        <InputLabel  id="demo-multiple-name-label">Simplified Market Segment</InputLabel>
        <Select
          fullWidth
          labelId="demo-multiple-name-label"
          id="demo-multiple-name"
          value={props.filterSMS}
          onChange={(e)=>{
            props.setFilterSMS(e.target.value)
          }}
          input={<OutlinedInput label="Name" />}
        >
          {props.filtersSMSList.map((name) => (
            <MenuItem
              key={name}
              value={name}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </Grid>
    </Grid>);
}