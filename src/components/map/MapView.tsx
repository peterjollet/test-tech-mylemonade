
import { useRef } from 'react';
import Map, {   GeoJSONSource, MapRef, Source, Layer } from 'react-map-gl';
import { IMapTestService } from '../../hooks/useMapControler';
import { clusterCountLayer, clusterLayer, unclusteredPointLayer } from './cluster/LayerCluster';

const MAP_TOKEN = 'pk.eyJ1IjoiZHRoaWIiLCJhIjoiY2tod2FjcWpiNWJkaDM1bDZ5b2ZqeGVweiJ9.cRnbp_ra6HirjBUG0byyNA'

export const MapView = (props:IMapTestService) => {

  const mapRef = useRef<MapRef>(null);
  // Gestion du zoom et du clustering de points
  const onClick = (event:any) => {
    const feature = event.features[0];    
    const clusterId = feature.properties.cluster_id;

    if(mapRef.current){
      const mapboxSource = mapRef.current.getSource('restaurants') as GeoJSONSource;
  
      mapboxSource.getClusterExpansionZoom(clusterId, (err, zoom) => {
        if (err) {
          return;
        }
        if(mapRef.current)
        mapRef.current.easeTo({
          center: feature.geometry.coordinates,
          zoom,
          duration: 500
        });
      });
      
      
    }
  }; 


  return (
<>
    <Map 
    id='carte'
    initialViewState={{
      longitude: 3.08694,
      latitude: 45.7797,
      zoom: 6 }}
    style={{  height: '75rem' }}
    mapStyle="mapbox://styles/mapbox/streets-v9"
    mapboxAccessToken={MAP_TOKEN}
    onClick={onClick}
    >
      <Source
          id="restaurants"
          type="geojson"
          data={props.dataFiltered}
          cluster={true}
          clusterMaxZoom={14}
          clusterRadius={50}
        >
          <Layer {...clusterLayer} />
          <Layer {...clusterCountLayer} />
          <Layer {...unclusteredPointLayer} />
        </Source>
        
    </Map>
      </>
  );

  
  };