/* eslint-disable testing-library/prefer-screen-queries */
import React from 'react';
import { getByTestId, render } from '@testing-library/react';
import App from './App';

test("App load map", () => {

  const { container } = render(<App />);
  const countValue = getByTestId(container, "carte");
  expect(countValue.textContent).toBe("1");
});
